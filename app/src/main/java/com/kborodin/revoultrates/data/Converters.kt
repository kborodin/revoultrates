package com.kborodin.revoultrates.data

import androidx.room.TypeConverter
import com.google.gson.Gson

class Converters {

    @TypeConverter
    fun stringToPairList(value: String): Any {
        val stringList = value.removeSurrounding("{", "}").split(",")
        val pairCurrenciesList = ArrayList<Pair<String, Double>>()
        stringList.forEach {
            val code = it.substringBefore(":").removeSurrounding("\"", "\"")
            val price = it.substringAfter(":").toDouble()
            pairCurrenciesList.add(Pair(code, price))
        }
        return pairCurrenciesList
    }

    @TypeConverter
    fun anyToString(value: Any): String {
        return Gson().toJson(value)
    }
}
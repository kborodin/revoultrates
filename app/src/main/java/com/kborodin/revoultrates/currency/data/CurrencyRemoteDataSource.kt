package com.kborodin.revoultrates.currency.data

import com.kborodin.revoultrates.api.BaseDataSource
import com.kborodin.revoultrates.api.CurrencyService
import javax.inject.Inject

class CurrencyRemoteDataSource @Inject constructor(private val service: CurrencyService) :
    BaseDataSource() {
    suspend fun fetchRatesForCurrency(currency: String) =
        getResult { service.getCurrency(currency) }
}
package com.kborodin.revoultrates.currency.data

import com.kborodin.revoultrates.data.resultLiveData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyRepository @Inject constructor(
    private val dao: CurrencyDao,
    private val remoteDataSource: CurrencyRemoteDataSource
) {

    fun getCurrencyRates(currency: String) = resultLiveData(
        databaseQuery = { dao.getCurrencyRates() },
        networkCall = { remoteDataSource.fetchRatesForCurrency(currency) },
        saveCallResult = { dao.insertAll(it) }
    )
}
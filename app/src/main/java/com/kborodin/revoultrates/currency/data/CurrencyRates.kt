package com.kborodin.revoultrates.currency.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.kborodin.revoultrates.data.Converters

@Entity(tableName = "currencies")
@TypeConverters(Converters::class)
data class CurrencyRates(
    @PrimaryKey
    @field:SerializedName("id")
    val id: Int = 1,
    @field:SerializedName("baseCurrency")
    val baseCurrency: String,
    @field:SerializedName("rates")
    val rates: Any
) {
}
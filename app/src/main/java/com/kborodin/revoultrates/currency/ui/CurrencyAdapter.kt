package com.kborodin.revoultrates.currency.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.kborodin.revoultrates.R
import com.kborodin.revoultrates.currency.data.Currency
import kotlinx.android.synthetic.main.item_currency_rates.view.*
import java.util.*

class CurrencyAdapter(private val context: Context, private val currenciesList: List<Currency>) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency_rates, parent, false)
        )
    }

    override fun getItemCount(): Int = currenciesList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.initViews(position)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val ivFlag: ImageView = itemView.ivFlag
        private val tvCurrencyCode: TextView = itemView.tvCurrencyCode
        private val tvCurrencyName: TextView = itemView.tvCurrencyName
        private val etInputField: EditText = itemView.etInputField

        fun initViews(itemPosition: Int) {
            val item = currenciesList[itemPosition]
            initFlagImage(item.code)
            tvCurrencyCode.text = item.code
            val currency =
                java.util.Currency.getAvailableCurrencies().find { it.currencyCode == item.code }
            tvCurrencyName.text = currency?.displayName
            etInputField.setText(item.value.toString())
        }

        private fun initFlagImage(code: String) {
            val imageDrawableId =
                context.resources.getIdentifier(
                    code.toLowerCase(Locale.ROOT),
                    "drawable",
                    context.packageName
                )
            Glide.with(context).load(imageDrawableId)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .apply(RequestOptions.circleCropTransform())
                .skipMemoryCache(false)
                .into(ivFlag)
        }
    }

}
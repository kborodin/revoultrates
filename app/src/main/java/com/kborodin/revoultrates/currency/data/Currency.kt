package com.kborodin.revoultrates.currency.data

data class Currency(val code: String, var value: Double)
package com.kborodin.revoultrates.currency.ui

import androidx.lifecycle.ViewModel
import com.kborodin.revoultrates.currency.data.CurrencyRepository
import javax.inject.Inject

class CurrencyViewModel @Inject constructor(repository: CurrencyRepository) : ViewModel() {
    companion object {
        const val BASE_CURRENCY = "EUR"
    }

    lateinit var currency: String

    val currencyRates by lazy {
        repository.getCurrencyRates(currency)
    }
}
package com.kborodin.revoultrates.currency.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kborodin.revoultrates.R
import com.kborodin.revoultrates.currency.data.CurrencyRates
import com.kborodin.revoultrates.data.Result
import com.kborodin.revoultrates.databinding.FragmentCurrencyBinding
import com.kborodin.revoultrates.util.getCurrencies
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

class CurrencyFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<CurrencyViewModel> {
        viewModelFactory
    }

    private lateinit var adapter: CurrencyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.currency = CurrencyViewModel.BASE_CURRENCY

        val binding = DataBindingUtil.inflate<FragmentCurrencyBinding>(
            inflater,
            R.layout.fragment_currency,
            container,
            false
        ).apply {
            lifecycleOwner = this@CurrencyFragment
        }
        subscribeUi(binding)
        return binding.root
    }

    private fun subscribeUi(binding: FragmentCurrencyBinding) {
        viewModel.currencyRates.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.let {
                        bindView(binding, it)
                    }
                    Timber.d("Result.Status.SUCCESS")
                }
                Result.Status.LOADING -> {
                    Timber.d("Result.Status.LOADING")
                }
                Result.Status.ERROR -> {
                    Timber.d("Result.Status.ERROR")
                }
            }
        })
    }

    private fun bindView(binding: FragmentCurrencyBinding, currencyRates: CurrencyRates) {
        currencyRates.apply {
            adapter = CurrencyAdapter(requireContext(), getCurrencies(this.rates, this.baseCurrency))
            binding.currencyRatesRecyclerView.adapter = adapter
            binding.currencyRatesRecyclerView.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        }
    }


}
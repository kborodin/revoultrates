package com.kborodin.revoultrates.currency.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currencies")
    fun getCurrencyRates(): LiveData<CurrencyRates>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(currencyRates: CurrencyRates)

}
package com.kborodin.revoultrates.api

import com.kborodin.revoultrates.currency.data.CurrencyRates
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyService {
    companion object {
        const val ENDPOINT = "https://hiring.revolut.codes/api/android/"
        const val LATEST = "latest"
        const val QUERY_BASE = "base"
    }

    @GET(LATEST)
    suspend fun getCurrency(@Query(QUERY_BASE) currency: String): Response<CurrencyRates>
}
package com.kborodin.revoultrates.util

import com.kborodin.revoultrates.currency.data.Currency
import timber.log.Timber

fun getCurrencies(currencyRates: Any, baseCurrency: String): ArrayList<Currency> {
    val listOfCurrencies = ArrayList<Currency>()
    listOfCurrencies.add(Currency(baseCurrency, 1.00))
    try {
        val currencyRatesList = currencyRates as ArrayList<*>
        currencyRatesList.forEach {
            if (it is Pair<*, *>) {
                listOfCurrencies.add(Currency(it.first as String, it.second as Double))
            }
        }
    } catch (e: ClassCastException) {
        Timber.e(e.localizedMessage)
        e.printStackTrace()
    }
    return listOfCurrencies
}


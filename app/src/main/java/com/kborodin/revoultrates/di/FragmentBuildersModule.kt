package com.kborodin.revoultrates.di

import com.kborodin.revoultrates.currency.ui.CurrencyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeCurrencyFragment(): CurrencyFragment
}

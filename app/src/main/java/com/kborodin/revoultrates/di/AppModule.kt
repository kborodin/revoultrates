package com.kborodin.revoultrates.di

import android.app.Application
import com.kborodin.revoultrates.api.CurrencyService
import com.kborodin.revoultrates.currency.data.CurrencyRemoteDataSource
import com.kborodin.revoultrates.data.AppDatabase
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class, CoreDataModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideCurrencyService(
        @CurrencyApi okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ) =
        provideService(okHttpClient, converterFactory, CurrencyService::class.java)

    @Singleton
    @Provides
    fun provideCurrencyRemoteDataSource(currencyService: CurrencyService) =
        CurrencyRemoteDataSource(currencyService)

    @Singleton
    @Provides
    fun provideCurrencyDb(app: Application) = AppDatabase.getInstance(app)

    @Singleton
    @Provides
    fun provideCurrencyRatesDao(db: AppDatabase) = db.currencyDao()

    @CoroutineScropeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    @CurrencyApi
    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    private fun createRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(CurrencyService.ENDPOINT)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    private fun <T> provideService(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory, clazz: Class<T>
    ): T {
        return createRetrofit(okHttpClient, converterFactory).create(clazz)
    }
}